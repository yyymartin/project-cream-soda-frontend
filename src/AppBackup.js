import React, { useEffect, useState } from 'react';
import Dropzone from 'react-dropzone';

import logo from './logo.png';
import searchBtn from './search.png';
import loadingGif from './loading.gif';
import makeup from './banner_makeup.png';
import skin from './banner_skincare.png';
import perfume from './banner_fragrance.png';

import Login from './component/Login.js';
import Login from './component/Login.js';
import Login from './component/Login.js';
import Login from './component/Login.js';
import Login from './component/Login.js';
import Login from './component/Login.js';
import Login from './component/Login.js';
import Login from './component/Login.js';
import Login from './component/Login.js';

import { useDispatch, useSelector } from 'react-redux';
import { Switch, Route, Link, useParams } from "react-router-dom";
import { 
  searchKeyword, 
  handleLogin, 
  handleRegister, 
  getCategory, 
  getYohoHotItem, 
  getBonjourHotItem,
  searchOffer, 
  setLoading, 
  getCommentList, 
  postComment, 
  deleteComment,
  updateFiles,
  clearFile
} from "./action";
import { useHistory } from 'react-router-dom/cjs/react-router-dom.min';
import { useDropzone } from 'react-dropzone';

function App() {
console.log("app");
  return (
    <div>
      <Head />
      <SearchBar />
      <Route path="/result/:keyword">
        <Result/>
      </Route>
      <Switch>
        <Route path="/offer/:offerId">
          <Offer />
        </Route>
        <Route path="/category/:categoryId">
          <SelectedCategory />
        </Route>
        <Route exact path="/">
          <Category />
          <BonjourHotItem/>
          <YohoHotItem/>
        </Route>
      </Switch>
    </div>
  )
}

function BonjourHotItem () {
  const loading = useSelector((state) => state.site.loading)
  const bonjourHotItemResult = useSelector((state=> state.site.bonjourHotItemResult))
  
  const dispatch = useDispatch();

  useEffect(()=>{
    dispatch(setLoading(true))
    dispatch(getBonjourHotItem("Hot"))
  },[])

  useEffect(() => {
    dispatch(setLoading(false))
  }, [bonjourHotItemResult])

  const result = bonjourHotItemResult.filter(eachOffer => eachOffer.platform === "Bonjour")

  return (
    <div>
      <div>Suggested to you from Bonjour</div>
      <div>{loading ? <div>{loadingGif}</div> : null }</div>
      <div>
        {
          result.slice(0, 6).map(eachOffer => {
            return (
              <Link to={`/offer/${eachOffer._id}`} key={eachOffer._id} >
                <img src={eachOffer.image} width="150" alt="search"/>
              </Link>
            );
          })
        }
      </div>
    </div>
  )
}

function YohoHotItem () {
  const loading = useSelector((state) => state.site.loading)
  const yohoResult = useSelector((state=> state.site.yohoHotItemResult))
  
  const dispatch = useDispatch();

  useEffect(()=>{
    dispatch(setLoading(true))
    dispatch(getYohoHotItem("hot"))
    console.log(yohoResult)
  },[])

  useEffect(() => {
    dispatch(setLoading(false))
  }, [yohoResult])

  const result = yohoResult.filter(eachOffer => eachOffer.platform === "Yoho")

  
  return (
    <div>
      <div>Suggested to you from Yoho</div>
      <div>{loading ? <div>{loadingGif}</div> : null }</div>
      <div>
        {
          result.slice(0, 6).map(eachOffer => {
            return (
              <Link to={`/offer/${eachOffer._id}`} key={eachOffer._id} >
                <img src={eachOffer.image} width="150" alt="search"/>
              </Link>
            );
          })
        }
      </div>
    </div>
  )
}


function Head() {
  const [isLogged, setIsLogged] = useState(false);
  const [selectedLogin, setselectedLogin] = useState(false);
  const [selectedRegister, setselectedRegister] = useState(false);

  const username = useSelector(function (state) {
    return state.site.name
  })
  const token = useSelector(function (state) {
    return state.site.token
  })


  const history = useHistory();

  useEffect(() => {
    console.log("Page Refeshed")
    if (username !== "") {
      setIsLogged(true)
    } else {
      setIsLogged(false)
    }
  }, [username])

  // useEffect(() => {
  //   console.log("Page Refeshed")
  // }, [token])

  const handleLogout = () => {
    localStorage.removeItem("token")
    history.go(0);
  }
  const toggleLoginState = () => {
    if (selectedLogin) {
      setselectedLogin(false)
    } else {
      setselectedLogin(true)
    }
  }
  const toggleRegisterState = () => {
    if (selectedRegister) {
      setselectedRegister(false)
    } else {
      setselectedRegister(true)
    }
  }
  const backToMainPage = () => {
    history.push("/");
  }

  console.log(isLogged)
  console.log(selectedLogin)
  console.log(selectedRegister)
  console.log(token)
  return (
    <div>
      <nav>
        <div>
          <img onClick={() => {
            backToMainPage();
          }} src={logo} width="250" alt="logo" />

          <div>
            <div>{isLogged ? <div>Welcome {username}</div> : <div></div>}</div>
            <div>{isLogged ? null : <button onClick={toggleLoginState}>Login</button>}</div>
            <div>
              {selectedLogin && !token ? <Login /> : null}
            </div>
            <div>{isLogged ? null : <button onClick={toggleRegisterState}>Register</button>}</div>
            <div>
              {selectedRegister && !token ? <Register /> : null}
            </div>
            <div>{isLogged ? <button onClick={() => handleLogout()}>Logout</button> : null}</div>
          </div>
        </div>
      </nav >
    </div >
  )
}
function Login() {
  const history = useHistory();
  const [userName, setUserName] = useState("");
  const [password, setPassword] = useState("");
  const [showErrorMess, setErrorMess] = useState(false);

  const errorMessage = useSelector(function (state) {
    return state.site.errorMessage
  })

  const dispatch = useDispatch();

  return (
    <div>
      <input value={userName} onChange={(e) => {
        setUserName(e.target.value);; setErrorMess(false)
      }} placeholder="Username"></input>
      <input value={password} onChange={(e) => { setPassword(e.target.value); setErrorMess(false) }} placeholder="Password"></input>
      <button onClick={() => {
        dispatch(handleLogin(userName, password));
        setErrorMess(true);
      }}>Login</button>
      <div>{showErrorMess ? <div>{errorMessage}</div> : null}</div>
    </div>
  )
}
function Register() {
  const [email, setEmail] = useState("");
  const [userName, setUserName] = useState("");
  const [password, setPassword] = useState("");
  const [showErrorMess, setErrorMess] = useState(false);
  const errorMessage = useSelector(function (state) {
    return state.site.errorMessage
  })

  const dispatch = useDispatch();

  return (
    <div>
      <input value={userName} onChange={(e) => { setUserName(e.target.value); setErrorMess(false) }} placeholder="Username"></input>
      <input value={password} onChange={(e) => { setPassword(e.target.value); setErrorMess(false) }} placeholder="Password"></input>
      <input value={email} onChange={(e) => { setEmail(e.target.value); setErrorMess(false) }} placeholder="Email"></input>
      <button onClick={() => { dispatch(handleRegister(email, userName, password)); setErrorMess(true) }}>Register</button>
      <div>{showErrorMess ? <div>{errorMessage}</div> : null}</div>
    </div>
  )
}

function SearchBar() {
  const [input, setInput] = useState("");

  return (
    <div>
      <input onChange={(e) => { setInput(e.target.value) }} placeholder="SKII cream"></input>
      <Link to={`/result/${input}`}>
        <img src={searchBtn} width="30" height="30" alt="search" />
      </Link>
    </div>
  )
}

function Result() {
  const { keyword } = useParams();
  const resultFromStore = useSelector((state) => state.site.result)
  const loading = useSelector((state) => state.site.loading)

  const dispatch = useDispatch();

  useEffect(()=>{
    dispatch(setLoading(true))
    dispatch(searchKeyword(keyword))
  },[keyword])

  useEffect(() => {
    dispatch(setLoading(false))
  }, [resultFromStore])

  const bonjourResult = resultFromStore.filter(eachOffer => eachOffer.platform === "Bonjour")
  console.log("bonjour Result below")
  console.log(bonjourResult)

  const yohoResult = resultFromStore.filter(eachOffer => eachOffer.platform === "Yoho")
  console.log("yoho Result below")
  console.log(yohoResult)

  return (
    <div>
      <div>{loading ? <img src={loadingGif} /> : null}</div>
      <div>
        {
          bonjourResult.map(eachOffer => {
            return (
              <div key={eachOffer._id}>
                <Link to={`/offer/${eachOffer._id}`}>
                  <img src={eachOffer.image} alt="result" />
                  <div>{eachOffer.brandAndProduct}</div>
                  <div>{eachOffer.price}</div>
                  <div>{eachOffer.platform}</div>
                </Link>
              </div>
            );
          })
        }
      </div>
      <div>
        {
          yohoResult.map(eachOffer => {
            return (
              <Link to={`/offer/${eachOffer._id}`} key={eachOffer._id} >
                <img src={eachOffer.image} alt="result" />
                <div>{eachOffer.brandAndProduct}</div>
                <div>{eachOffer.price}</div>
                <div>{eachOffer.platform}</div>
              </Link>
            );
          })
        }
      </div>
    </div>
    
  )
}

function Offer() {
  const { offerId } = useParams();
  const [ isLogged, setIsLogged ] = useState("");

  const offer = useSelector((state) => state.site.offer);
  const username = useSelector((state)=>state.site.name);

  const dispatch = useDispatch();

  useEffect(()=>{
    dispatch(searchOffer(offerId))
  },[offerId])

  useEffect(() => {
    if (username !== "") {
      setIsLogged(true)
    } else {
      setIsLogged(false)
    }
  }, [username] )

  return (
    <div>
      <div>
        <img src={offer.image} />
        <div>Product : {offer.brandAndProduct}</div>
        <div>Price : {offer.price}</div>
        <div>Platform : {offer.platform}</div>
        <a href={offer.url} target="_blank">Source Page</a>
      </div>
      <div>
        {
          isLogged ? 
          <div>
            <CommentInput/>
            <DropzoneInput/>
          </div> : null}
      </div>
      <CommentList/>
    </div>
  )
}

function CommentInput () {
  console.log("In CommentInput Component")
  const [commentInput, setCommentInput] = useState("");

  const username = useSelector((state)=>state.site.name);
  const offer = useSelector((state) => state.site.offer);
  const files = useSelector((state)=> state.site.files);
  // const errorMessage = useSelector((state) => state.site.commentError);

  const dispatch = useDispatch();

  console.log()

  return (
    <div>
      <div>Username: {username}</div>
      <textarea value={commentInput} cols="40" rows="5" onChange={(e) => { setCommentInput(e.target.value) }} placeholder="your comment here..."></textarea>
      <button onClick={() => {
          dispatch(postComment(offer._id, commentInput,files));
          dispatch(clearFile())
          setCommentInput("")
        }}>Post</button>

    </div>
  )
}

// function Reader () {
//   let input = React.createRef();
//   const [ file , setFile ] = useState(null)
  
//   const handleAdd = () => {
//     setFile(URL.createObjectURL(input.current.files[0]))
//   } 

//   return (
//       <div>
//         {/* <div>{ file!==null ? <img src={file} width="100" alt="HIHI"/> : null }</div> */}
//       </div>
//   )   
// }

function CommentList() {
  const { offerId } = useParams();

  const commentList = useSelector((state) => state.site.commentList);
  
  const dispatch = useDispatch();

  useEffect(()=>{
    dispatch(getCommentList(offerId));
  },[commentList]);

  const token = useSelector(function (state) {
    return state.site.token
  })
  
  return (
    <div>
      {
        commentList.map(eachComment => {
          // console.log(eachComment);
          return (
            <div key={eachComment.postedBy}>
              <div>{eachComment.postBy} said: </div>
              <div>{eachComment.content}</div>
              <div>{eachComment.image ? <img src={ "http://localhost:8000/" + eachComment.image} width="200"/> : null }</div>
              <div>{eachComment.postDate}</div>
              <div>
                {token ? <button onClick={()=>{dispatch(deleteComment(eachComment._id, eachComment.offerId))}}>Delete</button> :null}
              </div>
              <br/>
            </div>
          )
        })
      }
    </div>
  )
}

function DropzoneInput() {
  // const [files, setFiles] = useState([]);
  const files = useSelector((state)=>state.site.files);

  const dispatch = useDispatch();

  const onDrop = (acceptedFiles)=>{
    dispatch(updateFiles(acceptedFiles));
  }

  const {
    getRootProps,
    getInputProps,
  } = useDropzone({
    onDrop,
    accept: 'image/jpeg, image/png'
  });

  const thumbs = files.map(file => (
    <div key={file.name}>
      <img
        src={URL.createObjectURL(file)}
        alt={file.name}
      />
      <button onClick={()=>{dispatch(clearFile())}}>Remove File</button>
    </div>
  ));

  useEffect(() => () => {
    files.forEach(file => URL.revokeObjectURL(file.preview));
  }, [files]);

  console.log(files);

  return (
    <section>
      <div {...getRootProps({})}>
        <input {...getInputProps()} />
        <div>Drag and drop your images here.</div>
      </div>
      <aside>
        {thumbs}
      </aside>
    </section>
  )
}



function Category() {
  return (
    <div>
      <Link to="/category/makeup">
        <img src={makeup} width="300" alt="searh" />
      </Link>
      <Link to="/category/skincare">
        <img src={skin} width="300" alt="searh" />
      </Link>
      <Link to="/category/perfume">
        <img src={perfume} width="300" alt="searh" />
      </Link>
    </div>
  );
};

function SelectedCategory () {
  const { categoryId } = useParams();
  const loading = useSelector((state) => state.site.loading)
  const categoryResult = useSelector((state=> state.site.categoryResult))
  
  const dispatch = useDispatch();

  useEffect(()=>{
    dispatch(setLoading(true))
    dispatch(getCategory(categoryId))
  },[categoryId])

  useEffect(() => {
    dispatch(setLoading(false))
  }, [categoryResult])

  const bonjourResult = categoryResult.filter(eachOffer => eachOffer.platform === "Bonjour")
  const yohoResult = categoryResult.filter(eachOffer => eachOffer.platform === "Yoho")

  return (
    <div>
      <div>Category: {categoryId}</div>
      <div>{loading ? <div>{loadingGif}</div> : null }</div>
      <div>
        {
          bonjourResult.map(eachOffer => {
            return (
              <Link to={`/offer/${eachOffer._id}`} key={eachOffer._id} >
                <img src={eachOffer.image} width="150" alt="search"/>
                <div>{eachOffer.brandAndProduct}</div>
                <div>{eachOffer.price}</div>
              </Link>
            );
          })
        }
      </div>
      <div>
        {
          yohoResult.map(eachOffer => {
            return (
              <Link to={`/offer/${eachOffer._id}`} key={eachOffer._id} >
                <img src={eachOffer.image} width="150" alt="search"/>
                <div>{eachOffer.brandAndProduct}</div>
                <div>{eachOffer.price}</div>
              </Link>
            );
          })
        }
      </div>
    </div>
  )
}

export default App;
