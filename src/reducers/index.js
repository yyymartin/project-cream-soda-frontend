import { combineReducers } from 'redux';

const initalState = {
  token:localStorage.getItem("token"),
  id: "",
  name:localStorage.getItem("name"),
  memberType:"",
  loading: false,
  result: [],
  categoryResult: [],
  bonjourHotItemResult:[],
  yohoHotItemResult:[],
  offer: [],
  commentList: [],
  errorMessage: "",
  becomeMember: "",
  files:[],
  commentError:""
};
const site = function (state = initalState, action) {
  switch (action.type) {
    case "SEARCH_RESULT":
      return {
        ...state,
        result: action.payload
      };
    case "CATEGORY_RESULT":
      return {
        ...state,
        categoryResult: action.payload
      };
    case "BONJOUR_RESULT":
      return {
        ...state,
        bonjourHotItemResult: action.payload
      };
    case "YOHO_RESULT":
      return {
        ...state,
        yohoHotItemResult: action.payload
      };
    case "SET_LOADING":
      return {
        ...state,
        loading: action.payload
      };
    case "UPDATE_KEYWORD":
      return {
        ...state,
        keyword: action.payload
      };
    case "GET_COMMENT_LIST":
      return {
        ...state,
        commentList: action.payload
      };
    case "BECOME_MEMBER":
      return {
        ...state,
        errorMessage: action.payload 
      };
    case "ERROR_MESSAGE":
      return {
        ...state,
        errorMessage: action.payload 
      };
      case "WELCOME_MEMBER":
      return {
        ...state,
        name: action.payload 
      };
      case "ADD_TOKEN":
      return {
        ...state,
        token: action.payload 
      };
      case "TARGET_OFFER":
      return {
        ...state,
        offer: action.payload 
      };
      case "UPDATE_FILES":
      return {
        ...state,
        files: action.payload 
      };
      case "CLEAR_RESULTS":
        return {
          ...state,
          result: []
        };
      case "CLEAR_FILES":
      return {
        ...state,
        files: [] 
      };
      case "DELETECOMMENT_ERROR":
      return {
        ...state,
        commentError: action.payload
      };
      case "REMOVE_TOKEN":
      return {
        ...state,
        token:""
      };
      case "REMOVE_NAME":
      return {
        ...state,
        name:""
      };
    default:
      return state;
  }
}
const rootReducer = combineReducers({
  site: site
})
export default rootReducer