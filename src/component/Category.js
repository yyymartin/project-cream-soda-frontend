import React from 'react';
import { Slide } from 'react-slideshow-image';
import 'react-slideshow-image/dist/styles.css'


import makeup from '../img/banner_makeup.png';
import skin from '../img/banner_skincare.png';
import perfume from '../img/banner_fragrance.png';
import categoryCSS from './Category.module.css';
import { Link } from "react-router-dom";

function Category() {

  return (
    <Slide className={categoryCSS.wrapper} easing="ease">

      <Link to="/category/makeup">
        <div className={categoryCSS.eachSlide}>
          <div style={{'backgroundImage': `url(${makeup})`}}/>
        </div>
      </Link>

      <Link to="/category/skincare">
        <div className={categoryCSS.eachSlide}>
          <div style={{'backgroundImage': `url(${skin})`}}/>
        </div>
      </Link>

      <Link to="/category/fragrance">
        <div className={categoryCSS.eachSlide}>
          <div style={{'backgroundImage': `url(${perfume})`}}/>
        </div>
      </Link>

    </Slide>
  )
};

export default Category;
