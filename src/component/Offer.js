import React, { useEffect, useState } from 'react';

import CommentInput from './CommentInput.js';
import DropzoneInput from './DropzoneInput.js';
import CommentList from './CommentList.js';

import { useDispatch, useSelector } from 'react-redux';
import { useParams } from "react-router-dom";
import { searchOffer } from "../action";
import appModuleCss from "../App.module.css";

function Offer() {
  const { offerId } = useParams();
  //const [isLogged, setIsLogged] = useState("");
  const  isLogged = useSelector(state=>{
    return state.site.token!=="" && state.site.token!==null && state.site.token!==undefined
   })

   const commentList = useSelector((state) => state.site.commentList);

  const offer = useSelector((state) => state.site.offer);
  const username = useSelector((state) => state.site.name);

  const dispatch = useDispatch();

  useEffect(() => {
    dispatch(searchOffer(offerId))
  }, [offerId])

  // useEffect(() => {
  //   const localName = localStorage.getItem("name");
  //   const localToken = localStorage.getItem("token");
  //   if (localName && localToken) {
  //     setIsLogged(true)
  //   } else {
  //     setIsLogged(false)
  //   }
  // }, [username])

  // console.log(commentList.length);

  if(commentList.length >= 1) {
    return (
      <div className={appModuleCss.offerContainer}>
        <div className={appModuleCss.offerDetail}>
          <img className={appModuleCss.offerImg} src={offer.image} />
          <div className={appModuleCss.offerDetailContainer}>
            <div className={appModuleCss.offerProductName}>{offer.brandAndProduct}</div>
            <div className={appModuleCss.offerPrice}>{offer.price}</div>
            <div className={appModuleCss.offerPlatform}>Platform</div>
            <div className={appModuleCss.offerPlatformType}>{offer.platform}</div>
            <div className={appModuleCss.offerPlatform}>Sourece Page</div>
            <div className={appModuleCss.offerPlatformType}>
              <div>
              <a className={appModuleCss.clearUnderLine} href={offer.url} target="_blank">Click there</a>
              </div>
            </div>
          </div>
        </div>
        <div className={appModuleCss.offerCommentPart}>
          {
            isLogged ?
              <div className={appModuleCss.commentBigContainer}>
                <CommentInput />
              </div> : null}
        <div className={appModuleCss.commentListBigContainer}><CommentList /></div>
        </div>
      </div>
    )
  } else {
    return (
      <div className={appModuleCss.offerContainer}>
        <div className={appModuleCss.offerDetail}>
          <img className={appModuleCss.offerImg} src={offer.image} />
          <div className={appModuleCss.offerDetailContainer}>
            <div className={appModuleCss.offerProductName}>{offer.brandAndProduct}</div>
            <div className={appModuleCss.offerPrice}>{offer.price}</div>
            <div className={appModuleCss.offerPlatform}>Platform</div>
            <div className={appModuleCss.offerPlatformType}>{offer.platform}</div>
            <div className={appModuleCss.offerPlatform}>Sourece Page</div>
            <div className={appModuleCss.offerPlatformType}>
              <div>
              <a className={appModuleCss.clearUnderLine} href={offer.url} target="_blank">Click there</a>
              </div>
            </div>
          </div>
        </div>
        <div className={appModuleCss.offerCommentPart}>
          {
            isLogged ?
              <div className={appModuleCss.commentBigContainer}>
                <CommentInput />
              </div> : null}
        </div>
      </div>
    )
  }
}

export default Offer;
