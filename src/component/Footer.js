import React, { useEffect } from 'react';
import logo from '../img/logo.png';
import FooterCSS from './Footer.module.css';



function Footer() {
  return (
    <div className={FooterCSS.wrapper}>
      <div>
        <img className={FooterCSS.logo} src={logo}/>
        <div className={FooterCSS.create}>Created by Harris Tam and Martin Yau, Cozy Code Academy, 2022</div>
      </div>
        <div className={FooterCSS.contact}>
          <br/>
          <div>Harris Tam : harris_1025@hotmail.com</div>
          <br/>
          <div>Martin Yau : martinware@tuta.io</div>      
        </div>
    </div>
  )
}

export default Footer;