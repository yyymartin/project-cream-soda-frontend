import React, { useState } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import { handleLogin } from "../action";

import loginCSS from './Login.module.css';

function Login() {
  const [ userName, setUserName ] = useState("");
  const [ password, setPassword ] = useState("");
  const [ showErrorMess, setErrorMess ] = useState(false);

  const errorMessage = useSelector((state)=> state.site.errorMessage);

  const dispatch = useDispatch();

  return (
    <div className={loginCSS.inputWrapper}>
      <input className={loginCSS.input}
        value={userName} onChange={(e) => {
        setUserName(e.target.value);
        setErrorMess(false)
      }}
        placeholder="Username"></input>

      <input className={loginCSS.input}
        value={password} onChange={(e) => {
        setPassword(e.target.value);
        setErrorMess(false)
      }}
        placeholder="Password"></input>

      <div>{showErrorMess ? <div className={loginCSS.error} >{errorMessage.toUpperCase()}</div> : null}</div>

      <button className={loginCSS.button}
        onClick={() => {
        dispatch(handleLogin(userName, password));
        setErrorMess(true);
      }}>Confirm</button>
    </div>
  )
}

export default Login;
