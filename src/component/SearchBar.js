import React, { useState } from 'react';
import searchBtn from '../img/search.png';

import { Link } from "react-router-dom";
import searchBarCSS from "./SearchBar.module.css";

function SearchBar() {
  const [input, setInput] = useState("");

  return (
    <div className={searchBarCSS.searchWrapper}>

      <input className={searchBarCSS.input}
        onChange={(e) => {setInput(e.target.value)}}
        placeholder="SKII cream">
      </input>

      <Link to={`/result/${input}`}>
        <button className={searchBarCSS.btn}>Search</button>
      </Link>

    </div>
  )
}

export default SearchBar;

// function SearchBar() {
//   const [input, setInput] = useState("");

//   return (
//     <div className={searchBarCSS.searchWrapper}>

//       <input className={searchBarCSS.input}
//         onChange={(e) => {setInput(e.target.value)}}
//         placeholder="SKII cream">
//       </input>

//       <Link to={`/result/${input}`}>
//         <img src={searchBtn} width="30" height="30" alt="search" />
//       </Link>

//     </div>
//   )
// }

// export default SearchBar;
