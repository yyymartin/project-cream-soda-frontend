import React, { useState } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import { postComment, clearFile } from "../action";
import appModuleCss from "../App.module.css";
import DropzoneInput from './DropzoneInput.js';

function CommentInput () {
  // console.log("In CommentInput Component")
  const [commentInput, setCommentInput] = useState("");

  const username = useSelector((state)=>state.site.name);
  const offer = useSelector((state) => state.site.offer);
  const files = useSelector((state)=> state.site.files);

  const dispatch = useDispatch();

  return (
    <div>
      <div className={appModuleCss.commentListTitle}>Comment</div>
    <div className={appModuleCss.commentInputContainer}>
      <div className={appModuleCss.commentInputUsername}>
       {username[0].toUpperCase()}
      </div>

      <div className={appModuleCss.commentInputAndPostButton}>
      <div className={appModuleCss.commentInputBigUsername}>{username}</div>
      <DropzoneInput />
      <textarea className={appModuleCss.commentInputTextArea} value={commentInput} cols="40" rows="5" 
      onChange={(e) => { setCommentInput(e.target.value) }} 
      placeholder="your comment here..."></textarea>
      
      <button className={appModuleCss.commentInputPostButton} onClick={() => {
          dispatch(postComment(offer._id, commentInput,files));
          dispatch(clearFile())
          setCommentInput("")
        }}>Post</button>
        </div>

    </div>
    </div>
  )
}

export default CommentInput;
