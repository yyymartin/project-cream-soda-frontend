import React, { useEffect } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import { Link, useParams } from "react-router-dom";
import { getCategory, setLoading } from "../action";
import loadingGif from '../img/loading.gif';
import appModuleCss from "../App.module.css";

function SelectedCategory() {
  const { categoryId } = useParams();
  const loading = useSelector((state) => state.site.loading);
  const categoryResult = useSelector((state => state.site.categoryResult));

  const dispatch = useDispatch();

  useEffect(() => {
    dispatch(getCategory(categoryId))
  }, [categoryId]);

  const bonjourResult = categoryResult.filter(eachOffer => eachOffer.platform === "Bonjour");
  const yohoResult = categoryResult.filter(eachOffer => eachOffer.platform === "Yoho");

  if (bonjourResult.length >= 1 && yohoResult.length < 1) {
    return (
      <div>
        <div className={appModuleCss.loading}>{loading ? <img src={loadingGif} /> : null}</div>
        {!loading ? <div className={appModuleCss.onlyOnePlatformSearch}>Search By Boujour</div> : null}
        <div className={appModuleCss.container}>
          <div className={appModuleCss.onlyOnePlatform}>
            {
              bonjourResult.map(eachOffer => {
                return (
                  <div className={appModuleCss.onlyOnePlatformBorder} key={eachOffer._id}>
                    <Link className={appModuleCss.clearUnderLine} to={`/offer/${eachOffer._id}`}>
                      <img className={appModuleCss.bigImg} src={eachOffer.image} alt="result" />
                      <div className={appModuleCss.onlyOnePlatformProductName}>{eachOffer.brandAndProduct}</div>
                      <div className={appModuleCss.price}>{eachOffer.price}</div>
                      <div className={appModuleCss.platformName}>{eachOffer.platform}</div>
                    </Link>
                  </div>
                );
              })
            }
          </div>
        </div>
      </div>
    )
  } else if (bonjourResult.length < 1 && yohoResult.length >= 1) {
    return (
      <div>
        <div className={appModuleCss.loading}>{loading ? <img src={loadingGif} /> : null}</div>
        {!loading ? <div className={appModuleCss.onlyOnePlatformSearch}>Search By Yoho</div> : null}
        <div className={appModuleCss.container}>
          <div className={appModuleCss.onlyOnePlatform}>
            {
              yohoResult.map(eachOffer => {
                return (
                  <div className={appModuleCss.onlyOnePlatformBorder} key={eachOffer._id}>
                    <Link className={appModuleCss.clearUnderLine} to={`/offer/${eachOffer._id}`} key={eachOffer._id} >
                      <img className={appModuleCss.bigImg} src={eachOffer.image} alt="result" />
                      <div className={appModuleCss.onlyOnePlatformProductName}>{eachOffer.brandAndProduct}</div>
                      <div className={appModuleCss.price}>{eachOffer.price}</div>
                      <div className={appModuleCss.platformName}>{eachOffer.platform}</div>
                    </Link>
                  </div>
                );
              })
            }
          </div>
        </div>
      </div>
    )
  } else {
    return(
      <div>
        <div className={appModuleCss.loading}>{loading ? <img src={loadingGif} /> : null}</div>
        <div className={appModuleCss.container}>
        {!loading ? <div className={appModuleCss.twoPlaftformSearch}>Search By Boujour</div> : null}
          <div className={appModuleCss.boujourAllResult}>
            {
              bonjourResult.map(eachOffer => {
                return (
                  <div className={appModuleCss.border} key={eachOffer._id}>
                    <Link className={appModuleCss.clearUnderLine} to={`/offer/${eachOffer._id}`}>
                      <img className={appModuleCss.bigImg} src={eachOffer.image} alt="result" />
                      <div className={appModuleCss.productName}>{eachOffer.brandAndProduct}</div>
                      <div className={appModuleCss.price}>{eachOffer.price}</div>
                      <div className={appModuleCss.platformName}>{eachOffer.platform}</div>
                    </Link>
                  </div>
                );
              })
            }
          </div>
          {!loading ? <div className={appModuleCss.twoPlaftformSearch}>Search By Yoho</div> : null}
          <div className={appModuleCss.yohoAllResult}>
            {
              yohoResult.map(eachOffer => {
                return (
                  <div className={appModuleCss.border} key={eachOffer._id}>
                    <Link className={appModuleCss.clearUnderLine} to={`/offer/${eachOffer._id}`} key={eachOffer._id} >
                      <img className={appModuleCss.bigImg} src={eachOffer.image} alt="result" />
                      <div className={appModuleCss.productName}>{eachOffer.brandAndProduct}</div>
                      <div className={appModuleCss.price}>{eachOffer.price}</div>
                      <div className={appModuleCss.platformName}>{eachOffer.platform}</div>
                    </Link>
                  </div>
                );
              })
            }
          </div>
        </div>
      </div>
    )
  }

  // return (
  //   <div>
  //     <div className={appModuleCss.categoryId}>Category: {categoryId}</div>
  //     <div className={appModuleCss.loading}>{loading ? <img src={loadingGif} /> : null }</div>
  //     <div>
  //       <div className={appModuleCss.container}>
  //         <div className={appModuleCss.boujourAllResult}>
  //           {
  //             bonjourResult.map(eachOffer => {
  //               return (
  //                 <div className={appModuleCss.border}>
  //                   <Link className={appModuleCss.clearUnderLine} to={`/offer/${eachOffer._id}`} key={eachOffer._id} >
  //                     <img className={appModuleCss.bigImg} src={eachOffer.image} width="150" alt="search" />
  //                     <div className={appModuleCss.productName}>{eachOffer.brandAndProduct}</div>
  //                     <div className={appModuleCss.price}>{eachOffer.price}</div>
  //                   </Link>
  //                 </div>
  //               );
  //             })
  //           }
  //         </div>
  //         <div className={appModuleCss.yohoAllResult}>
  //           {
  //             yohoResult.map(eachOffer => {
  //               return (
  //                 <div className={appModuleCss.border}>
  //                 <Link className={appModuleCss.clearUnderLine} to={`/offer/${eachOffer._id}`} key={eachOffer._id} >
  //                   <img className={appModuleCss.bigImg} src={eachOffer.image} width="150" alt="search" />
  //                   <div className={appModuleCss.productName}>{eachOffer.brandAndProduct}</div>
  //                   <div className={appModuleCss.price}>{eachOffer.price}</div>
  //                 </Link>
  //                 </div>
  //               );
  //             })
  //           }
  //         </div>
  //       </div>
  //     </div>
  //   </div>
  // );
};

export default SelectedCategory;