import React, { useEffect } from 'react';

import { useDispatch, useSelector } from 'react-redux';
import { useParams } from "react-router-dom";
import { getCommentList, deleteComment } from "../action";
import appModuleCss from "../App.module.css";

function CommentList() {
  const { offerId } = useParams();
  const token = useSelector((state) => state.site.token);
  const commentList = useSelector((state) => state.site.commentList);


  const dispatch = useDispatch();

  useEffect(() => {
    dispatch(getCommentList(offerId));
  }, [commentList]);

  if(commentList.length < 1){
    return null;
  } else {
    return (
      <div>
        <div className={appModuleCss.commentListTitle}>Comment History</div>
        {
          commentList.map(eachComment => {
            // console.log(eachComment);
            return (
              <div key={eachComment.postedBy} className={appModuleCss.commentListContainer}>
                <div className={appModuleCss.commentInputUsername}>{eachComment.postBy[0].toUpperCase()}</div>
                <div className={appModuleCss.commentListSmallContainer}>
                  <div className={appModuleCss.commentListNameAndDate}>
                    <div className={appModuleCss.commentInputBigUsername}>{eachComment.postBy}</div>
                    <div className={appModuleCss.commentListDate}>{eachComment.postDate}</div>
                  </div>
                  <div className={appModuleCss.commentListOutline}>
                  <div>{eachComment.image ? <img className={appModuleCss.postCommentImg} src={"http://localhost:8000/" + eachComment.image}  /> : null}</div>
                  <div className={appModuleCss.commentListContent}>{eachComment.content}</div>
                  <div>
                    {token ? <button className={appModuleCss.commentListDelete} onClick={() => { dispatch(deleteComment(eachComment._id, eachComment.offerId)) }}>Delete</button> : null}
                  </div>
                  </div>
                  <br />
                </div>
              </div>
            );
          })
        }
      </div>
    );
  }
};

export default CommentList;
