import React, { useEffect } from 'react';
import loadingGif from '../img/loading.gif';
import { useDispatch, useSelector } from 'react-redux';
import { Link, useParams } from "react-router-dom";
import { searchKeyword, setLoading, clearResults } from "../action";
import appModuleCss from "../App.module.css";

console.log(loadingGif)

function Result() {
  const { keyword } = useParams();
  const resultFromStore = useSelector((state) => state.site.result);
  const loading = useSelector((state) => state.site.loading);
  const dispatch = useDispatch();

  useEffect(() => {
    dispatch(searchKeyword(keyword))
  }, [keyword]);

  const bonjourResult = resultFromStore.filter(eachOffer => eachOffer.platform === "Bonjour")
  const yohoResult = resultFromStore.filter(eachOffer => eachOffer.platform === "Yoho")

  // console.log(bonjourResult.length, yohoResult.length);

  if (bonjourResult.length >= 1 && yohoResult.length < 1) {
    return (
      <div>
        <div className={appModuleCss.loading}>{loading ? <img src={loadingGif} /> : null}</div>
        {!loading ? <div className={appModuleCss.onlyOnePlatformSearch}>Search By Boujour</div> : null}
        <div className={appModuleCss.container}>
          <div className={appModuleCss.onlyOnePlatform}>
            {
              bonjourResult.map(eachOffer => {
                return (
                  <div className={appModuleCss.onlyOnePlatformBorder} key={eachOffer._id}>
                    <Link className={appModuleCss.clearUnderLine} to={`/offer/${eachOffer._id}`}>
                      <img className={appModuleCss.bigImg} src={eachOffer.image} alt="result" />
                      <div className={appModuleCss.onlyOnePlatformProductName}>{eachOffer.brandAndProduct}</div>
                      <div className={appModuleCss.price}>{eachOffer.price}</div>
                      <div className={appModuleCss.platformName}>{eachOffer.platform}</div>
                    </Link>
                  </div>
                );
              })
            }
          </div>
        </div>
      </div>
    )
  } else if (bonjourResult.length < 1 && yohoResult.length >= 1) {
    return (
      <div>
        <div className={appModuleCss.loading}>{loading ? <img src={loadingGif} /> : null}</div>
        {!loading ? <div className={appModuleCss.onlyOnePlatformSearch}>Search By Yoho</div> : null}
        <div className={appModuleCss.container}>
          <div className={appModuleCss.onlyOnePlatform}>
            {
              yohoResult.map(eachOffer => {
                return (
                  <div className={appModuleCss.onlyOnePlatformBorder} key={eachOffer._id}>
                    <Link className={appModuleCss.clearUnderLine} to={`/offer/${eachOffer._id}`} key={eachOffer._id} >
                      <img className={appModuleCss.bigImg} src={eachOffer.image} alt="result" />
                      <div className={appModuleCss.onlyOnePlatformProductName}>{eachOffer.brandAndProduct}</div>
                      <div className={appModuleCss.price}>{eachOffer.price}</div>
                      <div className={appModuleCss.platformName}>{eachOffer.platform}</div>
                    </Link>
                  </div>
                );
              })
            }
          </div>
        </div>
      </div>
    )
  } else {
    return(
      <div>
        <div className={appModuleCss.loading}>{loading ? <img src={loadingGif} /> : null}</div>
        <div className={appModuleCss.container}>
        {!loading ? <div className={appModuleCss.twoPlaftformSearch}>Search By Boujour</div> : null}
          <div className={appModuleCss.boujourAllResult}>
            {
              bonjourResult.map(eachOffer => {
                console.log(eachOffer);
                return (
                  <div className={appModuleCss.border} key={eachOffer._id}>
                    <Link className={appModuleCss.clearUnderLine} to={`/offer/${eachOffer._id}`}>
                      <img className={appModuleCss.bigImg} src={eachOffer.image} alt="result" />
                      <div className={appModuleCss.productName}>{eachOffer.brandAndProduct}</div>
                      <div className={appModuleCss.price}>{eachOffer.price}</div>
                      <div className={appModuleCss.platformName}>{eachOffer.platform}</div>
                    </Link>
                  </div>
                );
              })
            }
          </div>
          {!loading ? <div className={appModuleCss.twoPlaftformSearch}>Search By Yoho</div> : null}
          <div className={appModuleCss.yohoAllResult}>
            {
              yohoResult.map(eachOffer => {
                return (
                  <div className={appModuleCss.border} key={eachOffer._id}>
                    <Link className={appModuleCss.clearUnderLine} to={`/offer/${eachOffer._id}`} key={eachOffer._id} >
                      <img className={appModuleCss.bigImg} src={eachOffer.image} alt="result" />
                      <div className={appModuleCss.productName}>{eachOffer.brandAndProduct}</div>
                      <div className={appModuleCss.price}>{eachOffer.price}</div>
                      <div className={appModuleCss.platformName}>{eachOffer.platform}</div>
                    </Link>
                  </div>
                );
              })
            }
          </div>
        </div>
      </div>
    )
  }


  // return (
  //   <div>
  //     <div className={appModuleCss.loading}>{loading ? <img src={loadingGif} /> : null}</div>
  //     <div className={appModuleCss.container}>
  //       <div className={appModuleCss.boujourAllResult}>
  //         {
  //           bonjourResult.map(eachOffer => {
  //             return (
  //               <div className={appModuleCss.border} key={eachOffer._id}>
  //                 <Link className={appModuleCss.clearUnderLine} to={`/offer/${eachOffer._id}`}>
  //                   <img className={appModuleCss.bigImg} src={eachOffer.image} alt="result" />
  //                   <div className={appModuleCss.productName}>{eachOffer.brandAndProduct}</div>
  //                   <div className={appModuleCss.price}>{eachOffer.price}</div>
  //                   <div className={appModuleCss.platformName}>{eachOffer.platform}</div>
  //                 </Link>
  //               </div>
  //             );
  //           })
  //         }
  //       </div>
  //       <div className={appModuleCss.yohoAllResult}>
  //         {
  //           yohoResult.map(eachOffer => {
  //             return (
  //               <div className={appModuleCss.border} key={eachOffer._id}>
  //                 <Link className={appModuleCss.clearUnderLine} to={`/offer/${eachOffer._id}`} key={eachOffer._id} >
  //                   <img className={appModuleCss.bigImg} src={eachOffer.image} alt="result" />
  //                   <div className={appModuleCss.productName}>{eachOffer.brandAndProduct}</div>
  //                   <div className={appModuleCss.price}>{eachOffer.price}</div>
  //                   <div className={appModuleCss.platformName}>{eachOffer.platform}</div>
  //                 </Link>
  //               </div>
  //             );
  //           })
  //         }
  //       </div>
  //     </div>
  //   </div>
  // )
}

export default Result;
