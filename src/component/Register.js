import React, { useState } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import { handleRegister } from "../action";
import registerCSS from "./Register.module.css"

function Register() {
  const [ email, setEmail ] = useState("");
  const [ userName, setUserName ] = useState("");
  const [ password, setPassword ] = useState("");
  const [ showErrorMess, setErrorMess ] = useState(false);

  const errorMessage = useSelector((state) => state.site.errorMessage );

  const dispatch = useDispatch();

  return (
    <div className={registerCSS.inputWrapper}>

      <input className={registerCSS.input}
        value={email} onChange={(e) => {
        setEmail(e.target.value);
        setErrorMess(false) }} 
        placeholder="Email"></input>

      <input className={registerCSS.input}
        value={userName} onChange={(e) => {
        setUserName(e.target.value);
        setErrorMess(false) }} 
        placeholder="Username"></input>

      <input className={registerCSS.input}
        value={password} onChange={(e) => {
        setPassword(e.target.value);
        setErrorMess(false) }}
        placeholder="Password"></input>

      <div>{showErrorMess ? <div className={registerCSS.error}>{errorMessage.toUpperCase()}</div> : null}</div>

      <button className={registerCSS.button}
        onClick={() => {
        dispatch(handleRegister(email, userName, password)); 
        setErrorMess(true)}}>Confirm</button>

    </div>
  );
};

export default Register;
