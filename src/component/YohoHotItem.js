import React, { useEffect } from 'react';
import loadingGif from '../img/loading.gif';
import { useDispatch, useSelector } from 'react-redux';
import { Link } from "react-router-dom";
import { getYohoHotItem } from "../action";
import yohoHotItemCSS from './YohoHotItem.module.css'

console.log(loadingGif)

function YohoHotItem () {
  const loading = useSelector((state) => state.site.loading)
  const yohoResult = useSelector((state=> state.site.yohoHotItemResult))
  
  const dispatch = useDispatch();
  

  useEffect(()=>{
    dispatch(getYohoHotItem("new"));
  },[]);

  const result = yohoResult.filter(eachOffer => eachOffer.platform === "Yoho")
  return (
    <div className={yohoHotItemCSS.wrapper}>
      <div className={yohoHotItemCSS.text}>Suggested to you from Yoho</div>
      <div>{loading ? <img src={loadingGif} /> : null }</div>
      <div className={yohoHotItemCSS.each}>
        {
          result.slice(0, 15).map(eachOffer => {
            return (
              <Link to={`/offer/${eachOffer._id}`} key={eachOffer._id} >
                <img className={yohoHotItemCSS.image} src={eachOffer.image} alt="search"/>
              </Link>
            );
          })
        }
      </div>
    </div>
  )
}

export default YohoHotItem;
