import React, { useEffect } from 'react';
import loadingGif from '../img/loading.gif';

import { useDispatch, useSelector } from 'react-redux';
import { Link } from "react-router-dom";
import { getBonjourHotItem } from "../action";

import bonjourHotItemCSS from './BonjourHotItem.module.css'


function BonjourHotItem () {
  const loading = useSelector((state) => state.site.loading)
  const bonjourHotItemResult = useSelector((state=> state.site.bonjourHotItemResult))
  
  const dispatch = useDispatch();

  useEffect(()=>{
    dispatch(getBonjourHotItem("new"))
  },[])

  const result = bonjourHotItemResult.filter(eachOffer => eachOffer.platform === "Bonjour")

  return (
    <div class={bonjourHotItemCSS.wrapper}>
      <div className={bonjourHotItemCSS.text}>Suggested to you from Bonjour</div>
      <div>{loading ? <img src={loadingGif} /> : null }</div>
      <div className={bonjourHotItemCSS.each}>
        {
          result.slice(0, 30).map(eachOffer => {
            return (
              <Link to={`/offer/${eachOffer._id}`} key={eachOffer._id} >
                <img className={bonjourHotItemCSS.image} src={eachOffer.image} alt="search"/>
              </Link>
            );
          })
        }
      </div>
    </div>
  )
}

export default BonjourHotItem;
