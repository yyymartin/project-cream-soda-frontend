import React, { useEffect } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import { updateFiles, clearFile } from "../action";
import { useDropzone } from 'react-dropzone';
import appModuleCss from "../App.module.css";

function DropzoneInput() {
  // const [files, setFiles] = useState([]);
  const files = useSelector((state)=>state.site.files);

  const dispatch = useDispatch();

  const onDrop = (acceptedFiles)=>{
    dispatch(updateFiles(acceptedFiles));
  }

  const {
    getRootProps,
    getInputProps,
  } = useDropzone({
    onDrop,
    accept: 'image/jpeg, image/png'
  });

  const thumbs = files.map(file => (
    <div className={appModuleCss.dropZonePreview} key={file.name}>
      <img className={appModuleCss.dropZonePreviewPic}
        src={URL.createObjectURL(file)}
        alt={file.name}
      />
      <div className={appModuleCss.dropZonePreviewRemoveButtonContainer}><button className={appModuleCss.dropZonePreviewRemoveButton} onClick={()=>{dispatch(clearFile())}}>Remove File</button></div>
    </div>
  ));

  useEffect(() => () => {
    files.forEach(file => URL.revokeObjectURL(file.preview));
  }, [files]);

  // console.log(files);

  return (
    <section>
      <div className={appModuleCss.dropZone} {...getRootProps({})}>
        <input {...getInputProps()} />
        <div>Drag and drop your images here.</div>
      </div>
      <aside>
        {thumbs}
      </aside>
    </section>
  )
}

export default DropzoneInput;
