import React, { useState } from 'react';
import logo from '../img/logo.png';

import Register from './Register.js';
import Login from './Login.js';

import { useDispatch, useSelector } from 'react-redux';
import { useHistory } from 'react-router-dom/cjs/react-router-dom.min';
import { removeToken } from '../action';
import { handleLogout } from "../action";

import  HeadCSS from './Head.module.css';

function Head() {
  const [selectedLogin, setselectedLogin] = useState(false);
  const [selectedRegister, setselectedRegister] = useState(false);
  
  const username = useSelector((state) => state.site.name);
  const token = useSelector((state) => state.site.token )
  const isLogged = !!token;

  const history = useHistory();
  const dispatch = useDispatch();

  // const handleLogout = () => {
  //   localStorage.removeItem("token")
  //   localStorage.removeItem("name")
  //   dispatch(removeToken())
  //   history.go(0);
  // }

  const toggleLoginState = () => {
    if (selectedLogin) {
      setselectedLogin(false)
    } else {
      setselectedLogin(true)
      setselectedRegister(false)
    }
  }
  const toggleRegisterState = () => {
    if (selectedRegister) {
      setselectedRegister(false)
    } else {
      setselectedRegister(true)
      setselectedLogin(false)
    }
  }
  const backToMainPage = () => {
    history.push("/");
  }

  console.log("isLogged : ",isLogged)
  console.log("Login Button Selected : ",selectedLogin)
  console.log("Register Button Selected : ", selectedRegister)
  console.log(username)
  return (
    <div className={HeadCSS.bigWrapper}>
      <div className={HeadCSS.wrapper}>
        <img onClick={() => {
          backToMainPage();
        }} src={logo} width="250" alt="logo" />

        <div className={HeadCSS.btnDropdownWrapper}>
          <div>
            <div className={HeadCSS.loginRegisterWrapper}>
              <div className={HeadCSS.greeting}>
                <div>{isLogged ? <div>Welcome, {username}</div> : null}</div>
              </div>
              <div>{isLogged ? null : <button onClick={toggleLoginState} className={HeadCSS.loginBtn}>Login</button>}</div> 
              <div>{isLogged ? null : <button onClick={toggleRegisterState} className={HeadCSS.registerBtn}>Register</button>}</div>
              <div>{isLogged ? <button onClick={() => dispatch(handleLogout())} className={HeadCSS.logoutBtn}>Logout</button> : null}</div>
            </div>
            <div>
              <div className={HeadCSS.dropdown}>{selectedLogin && !token ? <Login/> : null}</div>
              <div className={HeadCSS.dropdown}>{selectedRegister && !token ? <Register/> : null}</div>
            </div>
          </div>
        </div>
      </div>

    </div>
  )
}

export default Head;
