import React from 'react';

import Head from './component/Head.js';
import SearchBar from './component/SearchBar.js';
import Result from './component/Result.js';
import Offer from './component/Offer.js';
import Category from './component/Category.js';
import SelectedCategory from './component/SelectedCategory.js';
import BonjourHotItem from './component/BonjourHotItem.js';
import YohoHotItem from './component/YohoHotItem.js';
import Footer from './component/Footer.js';

import { Switch, Route } from "react-router-dom";

import  AppCSS from './App.module.css';

function App() {
console.log("app");
  return (
    <div>
      <Head />
      <SearchBar />
      <Route path="/result/:keyword">
        <Result/>
      </Route>
      <Switch>
        <Route path="/offer/:offerId">
          <Offer />
        </Route>
        <Route path="/category/:categoryId">
          <SelectedCategory />
        </Route>
        <Route exact path="/">
          <Category />
          <BonjourHotItem/>
          <YohoHotItem/>
        </Route>
      </Switch>
      <Footer/>
    </div>
  )
}

export default App;
