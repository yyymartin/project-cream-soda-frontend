export const searchKeyword = (keyword) => {
  return async (dispatch) => {
    dispatch(setLoading(true));
    let res = await fetch(`/product/keyword/${keyword}`, {
      method: "GET"
    });
    let data = await res.json();
    if (data) {
      dispatch(setLoading(false))
      dispatch({
        type: "SEARCH_RESULT",
        payload:data.dbData
      })

    }
  };
};
export const getCategory = (catergoryId) => {
  return async (dispatch) => {
    dispatch(setLoading(true));
    let res = await fetch(`/product/keyword/${catergoryId}`, {
      method: "GET"
    });
    let data = await res.json();
    if (data) {
      dispatch(setLoading(false));
      dispatch({
        type: "CATEGORY_RESULT",
        payload:data.dbData
      });
    }
  };
};

export const getBonjourHotItem = (keyword) => {
  return async (dispatch) => {
    dispatch(setLoading(true));
    let res = await fetch(`/product/keyword/${keyword}`, {
      method: "GET"
    });
    let data = await res.json();
    if (data) {
      dispatch(setLoading(false));
      dispatch({
        type: "BONJOUR_RESULT",
        payload:data.dbData
      });
      
    }
  };
};
export const getYohoHotItem = (keyword) => {
  return async (dispatch) => {
    dispatch(setLoading(true));
    let res = await fetch(`/product/keyword/${keyword}`, {
      method: "GET"
    });
    let data = await res.json();
    console.log(data)
    if (data) {
    dispatch(setLoading(false));
      dispatch({
        type: "YOHO_RESULT",
        payload:data.dbData
      });
    }
  };
};

export const searchOffer = (offerId) => {
  return async (dispatch) => {
    dispatch(setLoading(true));
    let res = await fetch(`/offer/${offerId}`, {
      method: "GET"
    });
    let data = await res.json();
    if (data.success) {
      console.log(data);
      dispatch(setLoading(false));
      dispatch({
        type: "TARGET_OFFER",
        payload:data.dbData[0]
      })
    }
  };
};

export const setLoading = (status) => {
  console.log(status)
  return (dispatch) => {
    dispatch({
      type: "SET_LOADING",
      payload:status
    })
  };
};

export const getCommentList = (offerId) => {
  return async (dispatch) => {
    let res = await fetch(`/offer/${offerId}/comment`, {
      method: "GET"
    });
    let data = await res.json();
    if (data.success) {
      dispatch({
        type: "GET_COMMENT_LIST",
        payload: data.dbData
      });
    }
  }
};
export const postComment = (offerId,content,files) => {
  return async (dispatch) => {
    let token = localStorage.getItem("token");
    const formData = new FormData();

     console.log(files[0])
     console.log(offerId,content,files);
    
    formData.append("content",content);
    formData.append("image",files[0]);
    
    console.log(token, files, formData);

    if(token){
      let res = await fetch(`/member/offer/${offerId}/comment`,{
        method:"POST",
        headers: {"Authorization":`Bearer ${token}`},
        body: formData
      });
      let data = await res.json();
      if(data.success){
        dispatch(getCommentList(offerId));
      }
    }else{
      dispatch({
        type: "BECOME_MEMBER",
        payload:"Become a member to post comment"
      })
    }
  }
};

export const updateFiles = (files) => {
  return {
    type:"UPDATE_FILES",
    payload:files
  }
};

export const clearFile = () => {
  return {
    type:"CLEAR_FILES"
  }
};
export const clearResults = () => {
  return {
    type:"CLEAR_RESULTS"
  }
};

export const removeToken = () => {
  return {
    type:"REMOVE_TOKEN"
  }
};

export const removeName = () =>{
  return {
    type:"REMOVE_NAME"
  }
}

export const handleLogout = () => {
  return async(dispatch) =>{
    localStorage.removeItem("token")
    localStorage.removeItem("name")
    dispatch(removeToken());
    dispatch(removeName());
  }
};

export const deleteComment = (commentId, offerId) => {
  console.log(commentId)
  return async (dispatch) => {
    let token = localStorage.getItem("token")
    console.log(token)
    if(token){
      let res = await fetch(`/member/delete/comment/${commentId}`,{
        method:"DELETE",
        headers: { "Content-Type": "application/json", "Authorization":`Bearer ${token}`}
      });
      let data = await res.json();
      console.log(data)
      if(data.success){
        dispatch(getCommentList(offerId));
      }else{
        alert("You can't delete this comment")
        dispatch({
          type: "DELETECOMMENT_ERROR",
          payload:data.message
        })
      }
    }else{
      alert("Please log in")
    }
  }
};

export const handleLogin = (userName, pw) => {
  return async (dispatch) => {
    let res = await fetch("/login", {
      method: "POST",
      headers: { "Content-Type": "application/json" },
      body: JSON.stringify({
        username: userName,
        password: pw,
      })
    });
    let data = await res.json();
    if (data.success) {
      console.log(data)
      localStorage.setItem("token", data.testOnly)
      localStorage.setItem("name", data.username)
      dispatch({
        type: "WELCOME_MEMBER",
        payload: data.username
      });
      dispatch({
        type: "ADD_TOKEN",
        payload: data.testOnly
      });
    } else {
      console.log(data.message)
      dispatch({
        type:"ERROR_MESSAGE",
        payload:data.message
      })
    }
  };
};

export const handleRegister = (email, userName, pw) => {
  return async (dispatch) => {
    let res = await fetch("/register", {
      method: "POST",
      headers: { "Content-Type": "application/json" },
      body: JSON.stringify({
        email: email,
        username: userName,
        password: pw,
      })
    });
    let data = await res.json();
    console.log(data);
    if (data.success) {
      // console.log(data.token)
      localStorage.setItem("token", data.token)
      localStorage.setItem("name", data.username)
      dispatch({
        type: "WELCOME_MEMBER",
        payload: data.username
      });
      dispatch({
        type: "ADD_TOKEN",
        payload: data.token
      });
    } else {
      dispatch({
        type:"ERROR_MESSAGE",
        payload:data.message
      })
    }
  };
};

